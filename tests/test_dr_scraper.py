import unittest
from datetime import date

from bs4 import BeautifulSoup

import dr_scraper
from dr_scraper import DegreeRuleScraper, build_reqnode_tree, get_requirements_block


class TestMiscMethods(unittest.TestCase):
    def test_get_requirements_block_no_div(self):
        """Exception is raised if containing <div> can't be found."""
        soup = BeautifulSoup('<html><head></head><body></body></html>', 'html5lib')
        self.assertRaisesRegex(ValueError, 'requirements block', get_requirements_block, *[soup])

    def test_build_reqnode_tree_no_header(self):
        """Exception is raised if header <h2> can't be found."""
        soup = BeautifulSoup(
                '<html><head></head><body><div id="study"></div></body></html>',
                'html5lib'
                )
        self.assertRaisesRegex(
                ValueError,
                'requirements header',
                build_reqnode_tree,
                *[get_requirements_block(soup)]
                )

    def test_build_reqnode_tree_basic_tree(self):
        """
        Process a simplified P&C page.
        * Locates the study block on the page
        * Builds a tree representation
        * Skips blank lines
        * Steps up and down levels correctly based on the indent level of the text
        * Stops when it hits the next <h2> tag
        * Stores margin values and text correctly
        """
        with open('tests/sample_html/basic_tree.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')

        req_tree = build_reqnode_tree(get_requirements_block(soup))
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['The Master of Computing:'], 0, []),
                    (['A minimum of 48 units'], 40, []),
                    (['36 units from completion of:'], 40, [
                        (['COMP6442', 'Software Construction'], 80, []),
                        (['COMP8260', 'Professional Practice 2'], 80, []),
                        (['COMP6445', 'Computing Research Methods'], 80, []),
                        (['COMP6331', 'Computer Networks'], 80, []),
                        (['COMP6420', 'Introduction to Data Management, Analysis and Security'],
                         80, []),
                        (['COMP6120', 'Software Engineering'], 80, []),
                        ]),
                    (['Either:'], 0, [(['30 units from completion of.'], 40, []), ]),
                    (['Or:'], 0, [(['6 units from completion of'], 40, []), ]),
                    ])
                )

    def test_build_reqnode_tree_basic_tree2(self):
        """
        Process a simplified P&C page with different structure.
        * Correctly handles indents that aren't multiples of 40
        * Builds a correct tree representation
        * Stops when it hits the end of the containing div
        """
        with open('tests/sample_html/basic_tree2.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')

        req_tree = build_reqnode_tree(get_requirements_block(soup))
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['The Bachelor of Advanced Computing:'], 0, []),
                    (['A maximum of 60 units'], 40, []),
                    (['A minimum of 24 units.'], 40, []),
                    (['54 units from completion of:'], 40, [
                        (['COMP1600', 'Foundations of Computing'], 80, []),
                        (['COMP2100', 'Software Design Methodologies'], 80, [])
                        ]),
                    (['6 units from completion of:'], 40, [
                        (['COMP1100', 'Programming as Problem Solving'], 80, []),
                        (['COMP1130', 'Programming as Problem Solving (Advanced)'], 80, [])
                        ]),
                    (['Either:'], 40, [(['12 units from completion of'], 80, [])]),
                    (['Or:'], 40, [
                        (['12 units from completion of:'], 80, [
                            (['ENGN3230', 'Engineering Innovation'], 120, []),
                            (['VCUG3001', 'Unravelling Complexity'], 120, []),
                            (['VCUG3002', 'Mobilising Research'], 120, [])
                            ])
                        ]),
                    (['48 units from completion of'], 40, [])
                    ])
                )

    def test_build_reqnode_tree_process_table(self):
        """
        Process a simplified P&C page with requirements laid out using tables.
        """
        with open('tests/sample_html/process_tables.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')

        req_tree = build_reqnode_tree(get_requirements_block(soup), header_id='requirements')
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['This specialisation requires the completion of 24 units, which must '
                      'consist of:'], 0, []),
                    (['A maximum of 12 units may come from completion of courses from the '
                      'following list:'], 0, [
                        (['Code', 'Title', 'Units', 'COMP6260', 'Foundations of Computing', '6',
                          'COMP6262', 'Logic', '6', 'COMP6320', 'Artificial Intelligence', '6'],
                         40,
                         [])]),
                    (['A minimum of 12 units must come from completion of courses from the '
                      'following list:'], 0, [
                        (['Code', 'Title', 'Units', 'COMP8420', 'Neural Networks, Deep Learning '
                          'and Bio-inspired Computing', '6', 'COMP8600', 'Statistical Machine '
                          'Learning', '6', 'COMP8620', 'Advanced Topics in Artificial '
                          'intelligence', '6', 'COMP8650', 'Advanced Topics in Machine Learning',
                          '6', 'COMP8670', 'Advanced Topics in Logic and Computation', '6',
                          'ENGN6528', 'Computer Vision', '6'],
                         40,
                         [])]),
                    ])
                )

    def test_build_reqnode_tree_bad_indent(self):
        """
        Process a page with mis-aligned requirements
        """
        with open('tests/sample_html/bad_indent.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')
        req_tree = build_reqnode_tree(get_requirements_block(soup))
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['Either:'], 0, [(['30 units from completion of'], 40, [])]),
                    (['Or:'], 0, [
                        (['24 units from completion of'], 40, [
                            (['Artificial Intelligence'], 80, []),
                            (['Data Science'], 80, []),
                            (['Human Centred Design and Software Development'], 80, [])
                            ])
                        ]),
                    (['Unless otherwise stated'], 40, [])
                    ])
                )

        with open('tests/sample_html/bad_indent2.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')
        req_tree = build_reqnode_tree(get_requirements_block(soup), header_id='requirements')
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['The specialisation requires completion of 24 units'], 0, []),
                    (['A minimum of 12 units'], 0, []),
                    (['A maximum of 12 units'], 40, [
                        (['COMP6353', 'Systems Engineering for Software Engineers'], 80, []),
                        (['COMP6461', 'Computer Graphics'], 80, [])
                        ]),
                    (['A minimum of 12 units'], 40, [
                        (['COMP8173', 'Software Engineering Processes'], 80, []),
                        (['COMP8190', 'Model-Driven Software Development'], 80, [])
                        ])
                    ])
                )

    def test_build_reqnode_tree_multi_requirement_paragraph(self):
        """
        Process a page with multiple requirements in a single paragraph.
        """
        with open('tests/sample_html/multi_req_para.html') as sample_file:
            soup = BeautifulSoup(sample_file.read(), 'html5lib')
        req_tree = build_reqnode_tree(get_requirements_block(soup))
        self.assertTupleEqual(
                req_tree.dump_tree(),
                ([''], -1, [
                    (['Either:'], 0, [
                        (['6 units from completion of', 'MATH1113', 'Mathematical Foundations'],
                         40, []),
                        (['6 units from completion of'], 40, [
                            (['STAT1003', 'Statistical Techniques'], 80, []),
                            (['STAT1008', 'Quantitative Research Methods'], 80, [])
                            ])
                        ])
                    ])
                )


class TestDegreeRuleScraper(unittest.TestCase):
    def test_init(self):
        self.assertRaises(FileNotFoundError, *[DegreeRuleScraper, 'Foo'])
        scraper = DegreeRuleScraper(
                'https://programsandcourses.anu.edu.au/{}/program/VCOMP'.format(date.today().year)
                )
        self.assertEqual(scraper.year, str(date.today().year))
        self.assertEqual(scraper.plan_type, 'program')
        self.assertEqual(scraper.plan_code, 'VCOMP')
        # Parse a more comlicated URL
        scraper = DegreeRuleScraper(
                'https://programsandcourses.anu.edu.au/program/7706XMCOMP#program-requirements'
                )
        self.assertEqual(scraper.year, date.today().year)
        self.assertEqual(scraper.plan_type, 'program')
        self.assertEqual(scraper.plan_code, '7706XMCOMP')

    def test_process_global_reqs(self):
        """
        Ensure global requirements processing picks up the overall unit requirement, plus any
        additional level or College based requirements.
        """
        scraper = DegreeRuleScraper('tests/sample_html/global_reqs.html', path_is_file=True)
        scraper.plan_code = 'AACOM'
        orders = scraper.build_program_order_struct().children
        self.assertEqual(len(orders), 4)
        self.assertEqual(
                'AACOM Bachelor of Advanced Computing (Honours): >= 192 units from courses '
                'matching None',
                str(orders[0])
                )
        # Check MAX and level requirement
        self.assertEqual(
                'AACOM Global unit values required by level: <= 60 units from courses matching ['
                'A-Z]{4}(1)\d{3}[A-Z]?',
                str(orders[1])
                )
        # Check MIN, multiple levels combined with subject area
        self.assertEqual(
                'AACOM Global unit values required by level: >= 24 units from courses matching ('
                'COMP)(4|6)\d{3}[A-Z]?',
                str(orders[2])
                )
        # Match College requirement
        self.assertEqual(
                'AACOM Global unit values required by College: >= 120 units from courses matching ('
                'ENGN|COMP)\d{4}[A-Z]?',
                str(orders[3])
                )

    def test_build_order_struct_table_subplan(self):
        """
        Build an order struct of a subplan that uses a tables layout
        """
        scraper = DegreeRuleScraper(
                'tests/sample_html/process_tables.html',
                header_id='requirements',
                path_is_file=True
                )
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 3)
        order = orders.children[0]
        self.assertEqual(order.code, 'VCOMP')
        self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN'])
        self.assertEqual(order.unit_value, 24)
        self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        order = orders.children[1]
        self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SET'])
        self.assertEqual(order.unit_value, 12)
        self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MAX'])
        order = orders.children[2]
        self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SET'])
        self.assertEqual(order.unit_value, 12)
        self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MIN'])

    def test_process_principal_requirement_single_course(self):
        """
        Process a single compulsory course requirement
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_course.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        order = orders.children[-1]
        self.assertEqual(order.code, 'VCOMP')
        self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_COURSE'])
        self.assertEqual(order.unit_value, 24)
        self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertEqual(repr(order.course_filter), "['COMP8800']")

    def test_process_principal_requirement_compulsory_set(self):
        """
        Process a compulsory set of courses requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/compulsory_set.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        order = orders.children[-1]
        self.assertEqual(order.code, 'VCOMP')
        self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_COMPULSORY_COURSES'])
        self.assertEqual(order.unit_value, 36)
        self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertEqual(repr(order.course_filter), "['COMP6442', 'COMP8260', 'COMP6445']")

    def test_process_principal_requirement_single_area(self):
        """
        Process a single study area requirement with min/max/equal unit requirements.
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_area.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 3)
        for order_id, order in enumerate(orders.children):
            with self.subTest(order_id=order_id, order=order):
                self.assertEqual(order.code, 'VCOMP')
                self.assertEqual(
                        order.title,
                        dr_scraper.ORDER_LABEL['PRINCIPAL_MINMAX_SINGLE_AREA']
                        )
                self.assertEqual(repr(order.course_filter), '(COMP)\d{4}[A-Z]?')
                if order_id == 0:
                    self.assertEqual(order.unit_value, 12)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MIN'])
                elif order_id == 1:
                    self.assertEqual(order.unit_value, 18)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MAX'])
                elif order_id == 2:
                    self.assertEqual(order.unit_value, 24)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])

    def test_process_principal_requirement_multiple_areas(self):
        """
        Process a multiple study area requirement with min/max/equal unit requirements.
        """
        scraper = DegreeRuleScraper('tests/sample_html/minmax_area.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 3)
        for order_id, order in enumerate(orders.children):
            with self.subTest(order_id=order_id, order=order):
                self.assertEqual(order.code, 'VCOMP')
                self.assertEqual(
                        order.title,
                        dr_scraper.ORDER_LABEL['PRINCIPAL_MINMAX_MULTIPLE_AREAS']
                        )
                self.assertEqual(repr(order.course_filter), '(COMP|ENGN)\d{4}[A-Z]?')
                if order_id == 0:
                    self.assertEqual(order.unit_value, 12)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MIN'])
                elif order_id == 1:
                    self.assertEqual(order.unit_value, 18)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MAX'])
                elif order_id == 2:
                    self.assertEqual(order.unit_value, 24)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])

    def test_process_principal_requirement_single_set(self):
        """
        Process a single set of courses requirement with min/max//equal unit requirements.
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_set.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 2)
        for order_id, order in enumerate(orders.children):
            with self.subTest(order_id=order_id, order=order):
                self.assertEqual(order.code, 'VCOMP')
                self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SET'])
                if order_id == 0:
                    self.assertEqual(order.unit_value, 12)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
                    self.assertEqual(
                            repr(order.course_filter),
                            "['ENGN3230', 'VCUG3001', 'VCUG3002']"
                            )
                elif order_id == 1:
                    self.assertEqual(order.unit_value, 6)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MIN'])
                    self.assertEqual(repr(order.course_filter), "['ENGN3230', 'VCUG3001']")

    def test_process_principal_requirement_single_course_multi(self):
        """
        Process a single course taken multiple times requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_course_multi.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 3)
        for order_id, order in enumerate(orders.children):
            with self.subTest(order_id=order_id, order=order):
                self.assertEqual(order.code, 'VCOMP')
                self.assertEqual(order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_ONE_TIMES_MANY'])
                if order_id == 0:
                    self.assertEqual(order.unit_value, 24)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
                    self.assertEqual(repr(order.course_filter), "['COMP8800']")
                elif order_id == 1:
                    self.assertEqual(order.unit_value, 12)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MAX'])
                    self.assertEqual(repr(order.course_filter), "['COMP8800']")
                elif order_id == 2:
                    self.assertEqual(order.unit_value, 18)
                    self.assertEqual(order.operator, dr_scraper.REQUIREMENT_OPERATORS['MIN'])
                    self.assertEqual(repr(order.course_filter), "['COMP8800']")

    def test_process_alternative_sets(self):
        """
        Process alternative sets of requirements.
        """
        scraper = DegreeRuleScraper('tests/sample_html/alternative_sets.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        alternatives_parent = orders.children[0]
        self.assertEqual(alternatives_parent.code, 'VCOMP')
        self.assertEqual(
                alternatives_parent.title,
                dr_scraper.ORDER_LABEL['PRINCIPAL_ALTERNATIVE_SETS']
                )
        self.assertEqual(alternatives_parent.unit_value, -1)
        self.assertEqual(alternatives_parent.operator, dr_scraper.REQUIREMENT_OPERATORS['OR'])
        self.assertEqual(len(alternatives_parent.children), 2)
        for child_order in alternatives_parent.children:
            with self.subTest(child_order=child_order):
                self.assertEqual(child_order.code, 'VCOMP')
                self.assertEqual(
                        child_order.title,
                        dr_scraper.ORDER_LABEL['PRINCIPAL_ALTERNATIVE_SETS']
                        )
                self.assertEqual(child_order.unit_value, -1)
                self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['AND'])
        container = alternatives_parent.children[0]
        self.assertEqual(len(container.children), 1)
        child_order = container.children[0]
        self.assertEqual(alternatives_parent.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_MINMAX_SINGLE_AREA'])
        container = alternatives_parent.children[1]
        self.assertEqual(len(container.children), 1)
        child_order = container.children[0]
        self.assertEqual(alternatives_parent.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SET'])

    def test_process_principal_requirement_electives(self):
        """
        Process global electives requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/electives.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['ELECTIVES'])
        self.assertEqual(child_order.unit_value, 48)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertEqual(repr(child_order.course_filter), '.*')

    def test_process_principal_requirement_single_specialisation(self):
        """
        Process single specialisation subplan requirement.
        """
        scraper = DegreeRuleScraper(
                'tests/sample_html/single_specialisation.html',
                path_is_file=True
                )
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'DTSC-SPEC')
        self.assertFalse(child_order.title)
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['AND'])
        self.assertIsNone(child_order.course_filter)
        # Check the actual subplan requirements were processed
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'DTSC-SPEC')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN'])
        self.assertEqual(child_order.unit_value, 24)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertIsNone(child_order.course_filter)
        child_order = orders.children[1]
        self.assertEqual(child_order.code, 'DTSC-SPEC')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_COMPULSORY_COURSES'])
        self.assertEqual(child_order.unit_value, 18)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertEqual(repr(child_order.course_filter), "['COMP8410', 'COMP8430', 'COMP6490']")

    def test_process_principal_requirement_single_major(self):
        """
        Process single major subplan requirement. We know subplan processing pretty much works
        from test_process_principal_requirement_single_specialisation() so only need basic checks.
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_major.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSEC-MAJ')
        self.assertFalse(child_order.title)
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['AND'])
        self.assertIsNone(child_order.course_filter)
        # Check the actual subplan requirements were processed
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSEC-MAJ')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN'])
        self.assertEqual(child_order.unit_value, 48)

    def test_process_principal_requirement_single_minor(self):
        """
        Process single minor subplan requirement. We know subplan processing pretty much works
        from test_process_principal_requirement_single_specialisation() so only need basic checks.
        """
        scraper = DegreeRuleScraper('tests/sample_html/single_minor.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSFN-MIN')
        self.assertFalse(child_order.title)
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['AND'])
        self.assertIsNone(child_order.course_filter)
        # Check the actual subplan requirements were processed
        self.assertEqual(len(child_order.children), 2)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSFN-MIN')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN'])
        self.assertEqual(child_order.unit_value, 24)

    def test_process_principal_requirement_specialisation_options(self):
        """
        Process specialisation alternatives subplan requirement.
        """
        scraper = DegreeRuleScraper(
                'tests/sample_html/specialisation_options.html',
                path_is_file=True
                )
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SUBPLAN_CHOICE'])
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['OR'])
        self.assertIsNone(child_order.course_filter)
        # Check a subplan order tree was added for each subplan option
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'INSY-SPEC')
        child_order = orders.children[1]
        self.assertEqual(child_order.code, 'SYAR-SPEC')
        child_order = orders.children[2]
        self.assertEqual(child_order.code, 'THCS-SPEC')

    def test_process_principal_requirement_major_options(self):
        """
        Process major alternatives subplan requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/major_options.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SUBPLAN_CHOICE'])
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['OR'])
        self.assertIsNone(child_order.course_filter)
        # Check a subplan order tree was added for each subplan option
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'BMSY-MAJ')
        child_order = orders.children[1]
        self.assertEqual(child_order.code, 'ELCO-MAJ')
        child_order = orders.children[2]
        self.assertEqual(child_order.code, 'MMSY-MAJ')

    def test_process_principal_requirement_minor_options(self):
        """
        Process minor alternatives subplan requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/minor_options.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SUBPLAN_CHOICE'])
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['OR'])
        self.assertIsNone(child_order.course_filter)
        # Check a subplan order tree was added for each subplan option
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'BMSY-MIN')
        child_order = orders.children[1]
        self.assertEqual(child_order.code, 'ELCO-MIN')
        child_order = orders.children[2]
        self.assertEqual(child_order.code, 'MMSY-MIN')

    def test_process_principal_requirement_specialisation_coreq(self):
        """
        Process undergrad specialisation that requires a major.
        """
        scraper = DegreeRuleScraper('tests/sample_html/specialisation_coreq.html',
                                    path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSEC-MAJ')
        self.assertFalse(child_order.title)
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['AND'])
        self.assertIsNone(child_order.course_filter)
        # Check the actual subplan requirements were processed
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'CSEC-MAJ')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN'])
        self.assertEqual(child_order.unit_value, 48)

    def test_process_principal_requirement_specialisation_coreq_options(self):
        """
        Process undergrad specialisation that requires a major chosen from a set of alternatives.
        """
        scraper = DegreeRuleScraper('tests/sample_html/specialisation_coreq_options.html',
                                    path_is_file=True)
        scraper.plan_code = 'VCOMP'
        scraper.path = 'https://programsandcourses.anu.edu.au/'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        # Check the root node of the subplan orders
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(
                child_order.title,
                dr_scraper.ORDER_LABEL['UGRAD_SPECIALISATION_COREQ_CHOICE']
                )
        self.assertEqual(child_order.unit_value, -1)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['OR'])
        self.assertIsNone(child_order.course_filter)
        # Check a subplan order tree was added for each subplan option
        self.assertEqual(len(child_order.children), 3)
        orders = child_order
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'BMSY-MAJ')
        child_order = orders.children[1]
        self.assertEqual(child_order.code, 'ELCO-MAJ')
        child_order = orders.children[2]
        self.assertEqual(child_order.code, 'MMSY-MAJ')

    def test_process_requirement_node_progression(self):
        """
        Process progression requirement.
        """
        scraper = DegreeRuleScraper('tests/sample_html/progression.html', path_is_file=True)
        scraper.plan_code = 'VCOMP'
        orders = scraper.build_program_order_struct()
        self.assertEqual(len(orders.children), 1)
        child_order = orders.children[0]
        self.assertEqual(child_order.code, 'VCOMP')
        self.assertEqual(child_order.title, dr_scraper.ORDER_LABEL['PROGRESSION'])
        self.assertEqual(child_order.unit_value, 0)
        self.assertEqual(child_order.operator, dr_scraper.REQUIREMENT_OPERATORS['=='])
        self.assertEqual(
                child_order.text,
                'Students who do not achieve a GPA of 6 in the first 48 units of courses '
                'attempted will be transferred to the Master of Computing.'
                )
