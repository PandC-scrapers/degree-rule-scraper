"""
Let's see where this goes...
"""
import re
from datetime import date
from itertools import chain
from math import ceil
from typing import Iterable
from urllib.parse import ParseResult, urlparse

import requests
from bs4 import BeautifulSoup
from bs4.element import NavigableString

COURSE_REGEX = r'[A-Z]{4}\d{4}[A-Z]?'
AREA_REGEX = r'[A-Z]{4}'
LEVEL_REGEX = r'\d{4}'
COLLEGE_CODES = {
    'Engineering and Computer Science': ('ENGN', 'COMP')
    }
PLAN_TYPE = {
    'PROGRAM': 'program',
    'MAJOR': 'major',
    'MINOR': 'minor',
    'SPECIALISATION': 'specialisation'
    }
REQUIREMENT_OPERATORS = {
    '==': '==',
    'MAX': '<=',
    'MIN': '>=',
    'AND': 'AND',
    'OR': 'OR',
    }
ORDER_LABEL = {
    'GLOBAL_BY_LEVEL': 'Global unit values required by level',
    'GLOBAL_BY COLLEGE': 'Global unit values required by College',
    'PRINCIPAL_SINGLE_COURSE': 'Single compulsory course',
    'PRINCIPAL_COMPULSORY_COURSES': 'Set of compulsory courses',
    'PRINCIPAL_MINMAX_MULTIPLE_AREAS': 'Unit value from multiple areas',
    'PRINCIPAL_MINMAX_SINGLE_AREA': 'Unit value from single area',
    'PRINCIPAL_SINGLE_SET': 'Unit value from set of courses',
    'PRINCIPAL_ONE_TIMES_MANY': 'Unit value from single course taken multiple times',
    'PRINCIPAL_SINGLE_SUBPLAN': 'Completion of a sub plan',
    'PRINCIPAL_SUBPLAN_CHOICE': 'Completion of a sub plan chosen from a set of alternatives',
    'UGRAD_SPECIALISATION_COREQ': 'Major required by specialisation',
    'UGRAD_SPECIALISATION_COREQ_CHOICE': 'Major chosen from alternatives required by '
                                         'specialisation',
    'PRINCIPAL_ALTERNATIVE_SETS': 'Select from alternative sets of requirements',
    'ELECTIVES': 'Electives only restricted by global requirements',
    'PROGRESSION': 'Progression depends on requirement text',
    'UNKNOWN_REQUIREMENT': 'Unknown requirement text'
    }
# This gets updated when course codes are seen during processing.
COURSE_CODES = {'COMP8173', 'ENGN1217', 'COMP7240', 'COMP1140', 'COMP6262', 'ENGN4511',
                'COMP3310', 'VCPG6001', 'COMP6250', 'ENGN8150', 'ENGN2228', 'ENGN3810',
                'COMP3620', 'ENGN2707', 'ENGN2222', 'ENGN3013', 'COMP4450', 'ENGN3224',
                'COMP1030', 'COMP8460', 'VCPG6100', 'COMP7310', 'COMP6461', 'ENGN2229',
                'ENGN2218', 'COMP2560', 'VCUG3001', 'ENGN6213', 'COMP5923', 'ENGN8833',
                'ENGN6536', 'ENGN8120', 'VCUG3100', 'ENGN6613', 'ENGN3230', 'ENGN3410',
                'COMP4560', 'COMP3650', 'COMP1600', 'ENGN3331', 'COMP1720', 'ENGN2225',
                'COMP6340', 'ENGN3200', 'ENGN8180', 'ENGN6511', 'COMP3320', 'COMP8830',
                'VCUG2004', 'ENGN6524', 'ENGN3334', 'ENGN6615', 'COMP4630', 'ENGN8526',
                'ENGN4420', 'COMP3701', 'COMP8300', 'ENGN6520', 'ENGN6420', 'COMP2310',
                'COMP6120', 'COMP4680', 'COMP6361', 'ENGN4536', 'COMP8755', 'ENGN4820',
                'ENGN6528', 'COMP6301', 'ENGN1211', 'ENGN6212', 'COMP3610', 'COMP4330',
                'COMP2410', 'COMP6710', 'COMP3900', 'COMP6331', 'COMP3430', 'ENGN3226',
                'COMP3550', 'ENGN4523', 'COMP8330', 'COMP8670', 'COMP2120', 'ENGN6334',
                'ENGN8100', 'COMP8440', 'COMP6420', 'VCUG2003', 'COMP3100', 'COMP6442',
                'VCUG3200', 'COMP7230', 'COMP8410', 'ENGN6410', 'COMP2550', 'VCPG8001',
                'COMP6330', 'COMP3300', 'ENGN4221', 'COMP8650', 'COMP2130', 'COMP2610',
                'ENGN3820', 'INFT4005F', 'COMP8600', 'ENGN8602', 'COMP4650', 'ENGN6525',
                'COMP8800', 'COMP6730', 'ENGN8537', 'ENGN6250', 'ENGN6224', 'ENGN6331',
                'COMP4130', 'COMP8320', 'COMP3425', 'COMP6719', 'ENGN4625', 'ENGN4525',
                'COMP6466', 'ENGN2226', 'ENGN6601', 'COMP4800', 'COMP8701', 'ENGN8104',
                'COMP6390', 'ENGN6537', 'COMP4670', 'ENGN4810', 'COMP3630', 'COMP2420',
                'ENGN4521', 'ENGN5923', 'ENGN8601', 'ENGN4520', 'ENGN6626', 'ENGN8536',
                'COMP3710', 'COMP3702', 'COMP8100', 'ENGN4513', 'COMP8180', 'COMP8715',
                'COMP4500', 'COMP3500', 'COMP6310', 'ENGN3512', 'COMP8620', 'ENGN6223',
                'COMP1100', 'ENGN4615', 'ENGN3221', 'ENGN6627', 'ENGN4613', 'COMP1710',
                'COMP4660', 'COMP6240', 'VCPG6004', 'COMP6353', 'COMP6490', 'COMP4610',
                'ENGN8831', 'ENGN6516', 'COMP3120', 'COMP3820', 'COMP6311', 'COMP4540',
                'ENGN5920', 'ENGN8820', 'COMP1130', 'INFT4005P', 'COMP6720', 'COMP4005P',
                'ENGN4528', 'COMP6780', 'COMP5920', 'ENGN4522', 'ENGN8823', 'COMP6260',
                'COMP8260', 'ENGN4627', 'VCUG2001', 'COMP2700', 'ENGN4718', 'ENGN8524',
                'ENGN8528', 'ENGN8534', 'COMP3530', 'COMP1110', 'ENGN4516', 'COMP4620',
                'COMP6320', 'COMP6300', 'ENGN2706', 'COMP8823', 'ENGN3223', 'ENGN6512',
                'COMP8820', 'ENGN3712', 'ENGN8637', 'COMP4340', 'VCUG1001', 'COMP3560',
                'COMP3600', 'COMP1040', 'ENGN8830', 'VCUG3002', 'COMP6445', 'ENGN8527',
                'COMP8501', 'COMP6363', 'COMP4005F', 'ENGN4027', 'COMP4006', 'ENGN8224',
                'ENGN4524', 'COMP1730', 'COMP7500', 'COMP6700', 'COMP2140', 'COMP8430',
                'COMP6464', 'COMP6365', 'COMP8190', 'COMP2710', 'ENGN8160', 'COMP3770',
                'COMP2300', 'VCPG6002', 'COMP4300', 'VCPG8002', 'ENGN3100', 'ENGN3601',
                'ENGN1218', 'ENGN2219', 'ENGN8170', 'ENGN6625', 'COMP2100', 'ENGN3213',
                'COMP6261', 'ENGN4706', 'COMP8110', 'ENGN2217', 'COMP6470', 'COMP8420',
                'COMP3740', 'COMP2400', 'ENGN3706', 'ENGN4712', 'COMP2620', 'ENGN8538',
                'ENGN8832', 'VCPG6200', 'ENGN6521', 'COMP4600', 'ENGN3212', 'ENGN1215',
                'ENGN8535', 'COMP8502', 'COMP4550', 'ENGN8260', 'COMP8705', 'ENGN4537',
                'ENGN4200'}


class ReqNode:
    """
    Represent a paragraph in a set of Program Orders on a Programs and Courses page.

    Store the text of the paragraph, as well as enough information about the position of this
    paragraph in the layout of the orders to give it context

    Program Orders are laid out (mostly) in a tree, with a couple of exceptions. These exceptions
    make it useful to be able to determine a particular tree node's siblings. As a result a node
    maintains links to the sibling to either side of it in the resultant tree structure.
    """
    next_sibling = None
    prev_sibling = None
    last_child = None

    def __init__(self, requirement, parent=None, children=None, margin=None):
        """
        :param requirement: The text or list of strings from the paragraph
        :param parent: Parent ReqNode if we want to set it here
        :param children: ReqNode at the head of a list of children
        :param margin: Value of the left-margin attribute of this paragraph in the HTML
        """
        self.requirement = requirement
        self.parent = parent
        if children and not isinstance(children, ReqNode):
            TypeError('Children parameter, if passed, must be an instance of ReqNode.')
        self.children = children
        if children:
            last_child = children
            while last_child.next_sibling:
                last_child = last_child.next_sibling
            self.last_child = last_child
        self.margin = margin

    def __iter__(self):
        current_sibling = self
        while current_sibling:
            yield current_sibling
            current_sibling = current_sibling.next_sibling

    def __str__(self):
        return self.flatten_requirement_text()

    @property
    def is_leaf(self):
        return self.children is None

    @staticmethod
    def _cut_family_ties(node):
        # If this node has existing relationships make a clean break
        if node.prev_sibling:
            node.prev_sibling.next_sibling = node.next_sibling
        if node.next_sibling:
            node.next_sibling.prev_sibling = node.prev_sibling
        node.parent = None
        node.next_sibling = None
        node.prev_sibling = None

    def append_child(self, new_child):
        """
        Append a new ReqNode to the end of the linked list of children.
        :param new_child: The ReqNode to append to the list
        """
        # Avoid complicated family structures
        self._cut_family_ties(new_child)
        # Append child
        if self.children is None:
            self.children = new_child
        else:
            self.last_child.next_sibling = new_child
            new_child.prev_sibling = self.last_child
        new_child.parent = self
        self.last_child = new_child

    def dump_tree(self):
        """Return a tuple/list representation of the tree under this node"""
        if self.children:
            return self.requirement, self.margin, [node.dump_tree() for node in self.children]
        return self.requirement, self.margin, []

    def flatten_requirement_text(self) -> str:
        """
        A ReqNode's requirement should be either a string or a list of strings. Attempt to
        flatten it.

        :return: The flattened string for the ReqNode's requirement
        """
        # If we get a requirement that isn't a string or iterable flail arms wildly
        if isinstance(self.requirement, str):
            return self.requirement
        else:
            return ' '.join(self.requirement)

    def flatten_children_text(self) -> str:
        """
        Usually the text detailing specific course codes etc. for a requirement is held in its
        children, either as multiple paragraphs in a single child or one paragraph per child,
        depending on the whim of whoever laid out the html for that particular page. Often we
        don't care about structure and just want it as a single string.

        :return: The flattened string.
        """
        return ' '.join([child.flatten_requirement_text() for child in self.children])

    def get_child_text_as_lines(self) ->Iterable[str]:
        """
        If there is more than one child return a list containing each child's text, or if there
        is a single child try to preserve line breaks in its text.

        :return: Iterable of the lines of text
        """
        if not self.children:
            return []
        if self.children.next_sibling:
            return [child.flatten_requirement_text() for child in self.children]
        if isinstance(self.children.requirement, str):
            return self.children.requirement.splitlines()
        return [line for line in self.children.requirement if line]


def _get_tag_indent(tag: BeautifulSoup, current_margin: int) -> int:
    """
    Visual structure is often created by adjusting the left margin of the <p> tags e.g.
    style="margin-left: 40px;", so extract that value to help navigate the page layout

    :param tag: HTML tag to extract alignment from
    :param current_margin: Alignment of previous tag so we can adjust for e.g <table> tags
    :return: The alignment of the tag
    """
    if 'style' in tag.attrs:
        match = re.search(r'(margin|padding)-left:\s?(?P<margin>-?\d{1,3})', tag['style'])
        if match:
            new_margin = int(match.group('margin'))
        else:
            raise ValueError("Couldn't determine alignment of requirement: {}".format(tag))
    elif tag.name == 'table':
        # An alternate page structure sometimes found on subplans uses <p> headings and then
        # lays out requirement contents like course lists using a <table>
        new_margin = current_margin + 40
    else:
        # No style attribute means we're not indented
        new_margin = 0
    return new_margin


def _split_multi_req_paragraph(tag: BeautifulSoup, new_margin: int) -> [BeautifulSoup]:
    """
    Apparently sometimes we just jam a whole bunch of requirements in a single <p> tag
    and use <br> to separate them. Restructure our html by walking the contents of this tag
    and separating its children into new paragraphs at the same indent when we hit a <br>
    tag. Delete the <br> tags as we encounter them.

    :param tag:
    :param new_margin:
    :return: A list of the newly created paragraphs
    """
    def _get_new_p_tag():
        return BeautifulSoup(
                '<p style="margin-left: {}px;"><p>'.format(new_margin),
                'html5lib'
                ).p.extract()

    new_paragraphs = []
    current_paragraph = tag  # type: BeautifulSoup
    br_tag = tag.br  # type: BeautifulSoup
    child = br_tag.next_sibling
    br_tag.decompose()
    new_p_tag = _get_new_p_tag()
    while child:
        next_child = child.next_sibling

        # If this is a <br> tag, if we've created a new paragraph with contents insert it
        # after the current paragraph then delete the <br> tag, so it doesn't mess with
        # stuff later.
        if child.name == 'br':
            # Check stripped_strings so we don't process an effectively empty line caused
            #  by adjacent <br> tags
            if list(new_p_tag.stripped_strings):
                current_paragraph.insert_after(new_p_tag)
                current_paragraph = new_p_tag
                new_paragraphs.append(new_p_tag)
                new_p_tag = _get_new_p_tag()
            child.decompose()
        else:
            new_p_tag.append(child)
        child = next_child

    if list(new_p_tag.stripped_strings):
        current_paragraph.insert_after(new_p_tag)
        new_paragraphs.append(new_p_tag)
    return new_paragraphs


def build_reqnode_tree(study_block: BeautifulSoup, header_id='program-requirements') -> ReqNode:
    """
    Construct a tree to replicate the structure of the Program Orders as rendered on P&C.

    There is just enough structure to the orders that we need to know their spatial relationship
    on the P&C page, but the html is constructed using <p> tags with margin shifts...

    Most of the time we don't care about the indents as most statements are just AND'd together
    at the top level. Exceptions are choice statements and capturing e.g. course lists that are
    part of certain requirements.

    :param study_block: The containing <div> for the requirements text.
    :param header_id: The ID of the html tag that is the heading for the actual requirements.
    :return: The root of the ReqNode tree structure representing the layout of the requirements.
    """
    root = ReqNode([''], margin=-1)
    current_parent = root
    tag = study_block.find(id=header_id)
    if tag is None:
        raise ValueError("Couldn't find the requirements header on the page, this isn't going "
                         "to work...")
    tag = tag.next_sibling
    current_margin = 0
    alternative_indent = 0
    principal_indent = 0
    processing_alternative = False
    processing_principal = False

    # Content is arranged between <h2> tags so keep grabbing stuff until we hit the end of
    # the <div> or the next <h2>
    while tag and tag.name != 'h2':
        # Empty tag, spacer, go to top link, skip it all
        if (isinstance(tag, NavigableString)
                or ('class' in tag.attrs and 'back-to-top' in tag['class'])):
            tag = tag.next_sibling
            continue

        new_margin = _get_tag_indent(tag, current_margin)

        # Of course there are different margin increments kicking around so normalise them to
        # something consistent, in this case multiples of 40 which is the biggest increment
        # encountered
        if new_margin > 0:
            new_margin = ceil(new_margin / 40) * 40

        # If we've passed the bottom of a requirement and its e.g. course list unset principal
        # requirement processing state.
        if processing_principal and new_margin <= principal_indent:
            processing_principal = False
            principal_indent = 0

        # If we were processing alternatives check if we've exited that block and unset that state.
        if processing_alternative and (
                new_margin <= alternative_indent
                or not ''.join(tag.stripped_strings)
                ):
            processing_alternative = False
            alternative_indent = 0
            current_parent = root

        # Apparently sometimes we just jam a whole bunch of requirements in a single <p> tag
        # and use <br> to separate them. Restructure our html by walking the contents of this tag
        # and separating its children into new paragraphs at the same indent when we hit a <br>
        # tag. Delete the <br> tags as we encounter them.
        if tag.br:
            new_paragraphs = _split_multi_req_paragraph(tag, new_margin)
            # It's also possible the new paragraphs are actually a list of courses that should be
            # indented underneath their parent requirement so try to detect this and adjust
            # margins accordingly
            new_lines = [' '.join(paragraph.stripped_strings) for paragraph in new_paragraphs]
            if (
                    len(new_paragraphs) > 1
                    and not re.match(COURSE_REGEX, ' '.join(tag.stripped_strings))
                    and all([re.match(COURSE_REGEX, line) for line in new_lines])
                    ):
                for paragraph in new_paragraphs:
                    paragraph['style'] = 'margin-left: {}px;'.format(new_margin + 40)

        requirement_text = ' '.join(tag.stripped_strings)

        # If it's a blank line then that typically indicates the end of a requirement block. Take
        # us back to the top level indent
        if new_margin < 0 or not ''.join(tag.stripped_strings):
            current_parent = root
            new_margin = 0
        # Try matching against different requirement regexes
        elif re.match(
                r'Th(e|is) (.*?) requires( the)? completion of (\d{1,3}) units.*?:?$',
                requirement_text
                ):
            root.append_child(ReqNode(list(tag.stripped_strings), margin=new_margin))
        elif re.match(r'(Either|Or):?$', requirement_text):
            # Adjust where the current parent is and set state that we're in a choice block
            new_node = ReqNode(list(tag.stripped_strings), margin=new_margin)
            root.append_child(new_node)
            current_parent = new_node
            alternative_indent = new_margin
            processing_alternative = True
        elif (
                re.match(r'(A maximum of |A minimum of )?\d{1,3} units', requirement_text)
                or re.match(
                    r'This specialisation must be taken in conjunction with',
                    requirement_text
                    )
                ):
            # Set state that we're processing a principal requirement so we can process any
            # children properly
            processing_principal = True
            principal_indent = new_margin
            new_node = ReqNode(list(tag.stripped_strings), margin=new_margin)
            current_parent.append_child(new_node)
        elif re.match(r'The \d{1,3} units must (consist of|include):$', requirement_text):
            pass
        else:
            new_node = ReqNode(list(tag.stripped_strings), margin=new_margin)
            # Either have course/subplan list for a principal requirement
            if processing_principal and new_margin > principal_indent:
                current_parent.last_child.append_child(new_node)
            # Or something else so we just store it
            else:
                current_parent.append_child(new_node)

        current_margin = new_margin
        tag = tag.next_sibling
    return root


def get_requirements_block(soup: BeautifulSoup) -> BeautifulSoup:
    """
    Locate the requirements block in the html document.

    :param soup: The BeautifulSoup object to search under
    :return: The containing element for all the requirements text
    """
    study_block = soup.find(id='study')
    if study_block is None:
        raise ValueError("Couldn't find a requirements block on the page, this isn't going to "
                         "work...")
    return study_block


# Basic representation of Course information
class Course:
    """
    Store some basic information about a course so we do do useful stuff with it later...
    """
    def __init__(self, code, unit_value, requisites):
        self.code = code
        self.unit_value = unit_value
        self.requisites = requisites


class CourseFilter:
    """
    Parent class of the various types of course filter.
    """
    def get_courses(self):
        raise NotImplementedError()


class CourseListFilter(CourseFilter):
    """Store an iterable of course codes"""
    def __init__(self, course_codes: Iterable[str]):
        super().__init__()
        self.course_codes = course_codes
        # Hack automatic collection of seen course codes
        COURSE_CODES.update(set(course_codes))

    def __repr__(self):
        return str(self.get_courses())

    def get_courses(self) -> list:
        """Returns the iterable of course codes stored in this object as a list."""
        if isinstance(self.course_codes, list):
            return self.course_codes
        return list(self.course_codes)


def build_course_regex(area_codes=None, levels=None) -> str:
    """
    Construct a regex that will filter desired course codes out of some list of codes.

    :param area_codes: A list of area codes e.g ['COMP']
    :param levels: A list of subject level prefix digits e.g. ['1', '4']
    :return: Regex string
    """
    if area_codes:
        area_prefix = '({})'.format('|'.join(area_codes))
    else:
        area_prefix = AREA_REGEX

    if levels:
        level_suffix = '({})'.format('|'.join(levels))
        level_suffix = level_suffix + r'\d{3}'
    else:
        level_suffix = LEVEL_REGEX

    return r'{}{}[A-Z]?'.format(area_prefix, level_suffix)


class CourseRegexFilter(CourseFilter):
    """Return a list of courses identified by a regular expression."""
    def __init__(self, course_regex: str):
        super().__init__()
        self.course_regex = course_regex

    def __repr__(self):
        return self.course_regex

    def get_courses(self, course_codes=None) -> list:
        """
        Filter an iterable of course codes based on this object's course_regex attribute.

        :param course_codes: Optional iterable of course codes. Uses COURSE_CODES if not set.
        :return: List of course codes that match self.course_regex.
        """
        if course_codes is None:
            course_codes = COURSE_CODES
        return [code for code in course_codes if re.match(self.course_regex, code)]


class ProgramOrder:
    """
    Details an individual requirement of a degree or similar degree plan level element.

    See https://policies.anu.edu.au/ppl/document/ANUP_006803 for details of Program orders.

    Program orders are a set of requirements which appear to follow the following recursive format:

    order ::= { requirement } ;
    requirement ::= { alternative } ;
    alternative ::= { requirement } ;

    A set of requirements are joined by a logical AND, alternatives by a logical OR. There is no
    negation at this level. An expanded order thus ends up as nested alternating AND/OR clauses e.g.

    req1 & req2 & (req3 | (req4 & re5 & (req6 | req7)) | req8) & req9

    Each requirement ultimately specifies some unit value that it contributes towards the
    completion of a degree and a set of courses that can be used to acquire those units,
    or some administrative hurdle/requirement. Courses define their own rules in terms of
    pre-requisites, co-requisites and incompatible courses etc. so at the Course level logical
    expressions will get more complex.

    At the moment there are really two types of ProgramOrder:
      * One that stores a specific requirement. These have no children and generally apply a unit
        value requirement to a set of courses via some min|max|equals operator.
      * One that is a container of sorts and groups ProgramOrders joined by an AND/OR operator.
    TODO: Split these up into a parent and subclasses so the role is explicit.

    The resulting representation is a tree starting with a container ProgramOrder, and it will
    have children that may themselves be containers, or could be specific requirement ProgramOrders.
    """
    def __init__(self, code: str, title: str, text: str, unit_value: int, operator: str,
                 course_filter=None):
        """
        :param code: The program/plan code this ProgramOrder belongs to.
        :param title: An entry in the PROGRAM_ORDER dict that describes this requirement.
        :param text: The raw text that was process to create this ProgramOrder.
        :param unit_value: Unit value which together with the operator and course_filter
        determines if this requirement is met.
        :param operator: Whether the unit value is and exact/max/min requirement.
        :param course_filter: Optional CourseFilter object that describes courses that can be
        used to fulfill the requirement of this ProgramOrder.
        """
        self.code = code
        self.title = title
        self.text = text
        self.unit_value = unit_value
        self.operator = operator
        self.course_filter = course_filter
        self.children = list()
        self.parent = None

    def __str__(self):
        return '{} {}: {} {} units from courses matching {}'.format(
                self.code,
                self.title,
                self.operator,
                self.unit_value,
                self.course_filter
                )

    def add_child(self, new_order):
        if not isinstance(new_order, ProgramOrder):
            raise ValueError("Can't add a non ProgramOrder as a child: {}".format(new_order))
        new_order.parent = self
        self.children.append(new_order)

    def dump_requirements(self):
        """
        Example method that recurses through the ProgramOrder tree and builds up a string
        describing the program's requirements in a sort-of propositional logic statement format.

        :return:
        """
        # NOT TESTED FOR ROBUSTNESS!!!
        if self.children:
            join_string = ' {} '.format(self.operator)
            return '({})'.format(
                    join_string.join([order.dump_requirements() for order in self.children])
                    )
        return '{} {} units from {}'.format(self.operator, self.unit_value, self.course_filter)


class DegreeRuleScraper:
    """
    Attempt to scrape ANU Program Orders from a html source and create a representation amenable
    to further manipulation.

    At the moment the main interface to the scraper is the build_program_order_struct() method
    which returns a ProgramOrder object, which is the root of a tree of ProgramOrders.
    """
    def __init__(self, path: str, header_id='program-requirements', plan_type=None, plan_code=None,
                 path_is_file=False):
        """
        :param path: The path of the html resource for this scraper to process. By default the
        scraper assumes this is a URL and tries to request it. If the URL is invalid it assumes
        it is a file path and tries to read the file. If you want to force treating the path as a
        file set path_if_file True.
        :param plan_type: Whether the plan is a progam|major|minor|specialisation
        :param plan_code: The plan code string e.g. MCOMP, ABCD-MAJ etc.
        :param header_id: HTML id of the tag that is the heading for the actual requirements.
        :param path_is_file: Force the scraper to use the path as a file rather than a URL.
        """
        # Extract the year, plan type and code from the path
        self.path = path
        self.plan_type = plan_type
        self.plan_code = plan_code
        self.header_id = header_id
        self.year = date.today().year
        self.path_is_file = path_is_file

        # Try to get some html...
        if path_is_file:
            with open(path) as source_file:
                html = source_file.read()
        else:
            try:
                html = requests.get(path).text
            # requests module URL format errors are all subclasses of ValueError so catch them
            # here and try to process as a file path. Other exceptions should propagate.
            except ValueError:
                with open(path) as source_file:
                    html = source_file.read()

            # Assuming the path was a URL try to extract some useful info.
            parse_result = urlparse(path)
            matches = re.search(
                    r'/?(?P<year>\d{4})?/(?P<plan_type>[a-z]+)/(?P<plan_code>[A-Z\d-]+)$',
                    parse_result.path
                    )
            if matches:
                self.year = matches.group('year') or self.year
                self.plan_type, self.plan_code = matches.group('plan_type', 'plan_code')

        self.soup = BeautifulSoup(html, 'html5lib')
        self.reqnode_tree = build_reqnode_tree(get_requirements_block(self.soup), self.header_id)

    def __repr__(self):
        return '{}, header_id={}, plan_type={}, plan_code={}, path_is_file={}'.format(
                self.path,
                self.header_id,
                self.plan_type,
                self.plan_code,
                self.path_is_file
                )

    def __str__(self):
        return self.path

    def build_subplan_url(self, subplan_url: str) -> str:
        """
        Build an absolute path for a subplan reference in the requirements of another program/plan.
        :param subplan_url: The relative path extracted from the links on a program/plan page.
        :return: The absolute path for the subplan
        """
        parsed_subplan = urlparse(subplan_url)
        parsed_base = urlparse(self.path)
        return ParseResult(parsed_base.scheme, parsed_base.netloc, parsed_subplan.path,
                           '', '', '').geturl()

    def get_subplan_header_(self, subplan_type: str) -> BeautifulSoup:
        """
        Get the subplan header element in the study block.

        :param subplan_type: Subplan type
        :return:
        """
        subplan_type = subplan_type.lower()
        if not subplan_type.endswith('s'):
            subplan_type += 's'
        study_block = get_requirements_block(self.soup)
        return study_block.find('h2', id=subplan_type)

    @staticmethod
    def get_subplan_links(subplan_header: BeautifulSoup):
        """
        Get a dict of <display_text>: <path> entries for the links in a particular subplan block.
        :param subplan_header:
        :return:
        """
        links = {}
        if subplan_header.next_sibling:
            tag = subplan_header.next_sibling
            # Skip over any e.g. newline strings.
            while tag and isinstance(tag, NavigableString):
                tag = tag.next_sibling

            for descendant in tag.descendants:
                if descendant.name == 'a':
                    links[' '.join(descendant.stripped_strings)] = descendant['href']
        return links

    def get_subplan_url(self, subplan_title: str, subplan_type: str):
        """
        Get a link to a specific subplan.

        :param subplan_title:
        :param subplan_type:
        :return:
        """
        subplan_links = self.get_subplan_links(self.get_subplan_header_(subplan_type))
        return subplan_links[subplan_title]

    @staticmethod
    def _minmax_operator(re_match):
        """
        Get the operator to apply to this requirement from the re match group.
        :param re_match: A re module match result
        :return: An operator from REQUIREMENT_OPERATORS
        """
        # Catch IndexError exceptions as we don't always match on both min and max.
        try:
            if re_match.group('max'):
                return REQUIREMENT_OPERATORS['MAX']
        except IndexError:
            pass
        try:
            if re_match.group('min'):
                return REQUIREMENT_OPERATORS['MIN']
        except IndexError:
            pass
        return REQUIREMENT_OPERATORS['==']

    @staticmethod
    def _extract_child_text(node: ReqNode) -> str:
        """
        For requirements that reference sets of some kind, sometimes these aren't indented
        properly so attempt to extract the appropriate text from children or siblings.

        :param node: The node that should have children, but might not.
        :return: The text we extracted from child nodes, or maybe siblings
        """
        if node.children:
            return node.flatten_children_text()
        # Node should have children but doesn't, process sibling text while it looks like a
        # course or study area in a list
        sibling = node.next_sibling
        sibling_text = sibling and sibling.flatten_requirement_text()
        child_text = []
        while sibling_text and re.match(AREA_REGEX, sibling_text):
            child_text.append(sibling_text)
            sibling = sibling.next_sibling
            sibling_text = sibling and sibling.flatten_requirement_text()
        return ' '.join(child_text)

    def process_global_requirements(self, node: ReqNode) -> Iterable[ProgramOrder]:
        """
        Global requirements need a little bit more processing so do that here.

        :param node: Node that matches a global requirement regex
        :return: The requirements for the program/plan
        """
        requirement_text = node.flatten_requirement_text()
        # Get the overall unit value requirements
        matches = re.match(
                r'Th(e|is) (?P<plan_title>.*?) requires( the)? completion of (?P<units>\d{1,'
                r'3}) units(, )?(?P<more_globals>of which)?.*?:?$',
                requirement_text
                )
        if matches:
            title = matches.group('plan_title')
            units = int(matches.group('units'))
        else:
            raise ValueError("Couldn't find a properly formatted global requirements header.")

        operator = REQUIREMENT_OPERATORS['MIN']
        if title in ('major', 'minor', 'specialisation'):
            title = ORDER_LABEL['PRINCIPAL_SINGLE_SUBPLAN']
            operator = REQUIREMENT_OPERATORS['==']

        new_order = ProgramOrder(
                self.plan_code,
                title,
                requirement_text,
                units,
                operator
                )
        yield new_order
        # Process requirements that are the children of a global statement
        if node.children:
            for child_node in node.children:
                for new_order in self.process_requirement_node(child_node):
                    yield new_order

    def process_alternative_sets(self, node: ReqNode) -> Iterable[ProgramOrder]:
        """
        Process a requirement node that is represented by alternative sets of principal
        requirements.

        :param node: The requirement node that starts the alternating sets.
        :return: The ProgramOrder containing the sets of alternative requirements.
        """
        alternative_orders = ProgramOrder(
                self.plan_code,
                ORDER_LABEL['PRINCIPAL_ALTERNATIVE_SETS'],
                node.flatten_requirement_text(),
                -1,
                REQUIREMENT_OPERATORS['OR']
                )
        # Construct a list of the start node of each set of alternative requirements.
        alternatives = [node.children]
        sibling = node.next_sibling
        while sibling and re.match(r'Or:?$', sibling.flatten_requirement_text()):
            alternatives.append(sibling.children)
            sibling = sibling.next_sibling
        # Go through the alternatives and process each set of requirements
        for alternative in alternatives:
            alternative_container = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_ALTERNATIVE_SETS'],
                    '',
                    -1,
                    REQUIREMENT_OPERATORS['AND']
                    )
            # Add each requirement from the set to its container ProgramOrder
            for principal_req in alternative:
                alternative_container.add_child(
                        self.process_principal_requirement(principal_req)
                        )
            alternative_orders.add_child(alternative_container)
        return [alternative_orders]

    def process_principal_requirement(self, node: ReqNode) -> ProgramOrder:
        """
        Process nodes that mostly result in pretty straight forward rules...

        WARNING: Ginormous set of if statements follows

        :param node: ReqNode to process
        :return: Program Orders extracted from this node and its children
        """
        # o_O O_o ... O_O
        principal_req_regex = {
            'single_course': r'(?P<units>\d{1,3}) units from(?: the)? completion of (?P<code>['
                             r'A-Z]{4}\d{4}[A-Z]?)',
            'compulsory_set': r'(?P<units>\d{1,3}) units from(?: the)? completion of the following '
                              r'(?:compulsory )?course\(?s\)?',
            'single_area': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,'
                           r'3}) units from(?: the)? completion of(?: further)? courses from the '
                           r'subject area (?P<area>[A-Z]{4}) ',
            'multiple_areas': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,'
                              r'3}) units from(?: the)? completion of courses from the following '
                              r'subject areas',
            'single_set': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,3}) '
                          r'units(?: may come| must come)? from(?: the)? completion of.*? courses? '
                          r'from the following(?: list)?',
            'single_set2': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,3}) '
                           r'units(?: may come| must come)? from( one of)?(?: the)? following.*? '
                           r'courses?',
            'single_course_multi': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,'
                                   r'3}) units from(?: the)? completion of (?P<code>[A-Z]{4}\d{4}['
                                   r'A-Z]?).*?, which (?:may|must) be completed more than once('
                                   r'?P<topic>, in a different topic in each instance)?('
                                   r'?P<consecutive>, in consecutive semesters)?',
            'single_subplan': r'\d{1,3} units from(?: the)? completion of the (?P<title>.*?) '
                              r'(?P<subplan>major|minor|specialisation)',
            'subplan_choice': r'\d{1,3} units from(?: the)? completion of one of the following '
                              r'(?:.*?)(?P<subplan>majors|minors|specialisations)',
            'specialisation_coreq': r'This specialisation must be taken in conjunction with the ('
                                    r'?P<title>.*?) major',
            'specialisation_coreq_choice': r'This specialisation must be taken in conjunction with '
                                           r'a major from the following list',
            'electives': r'(?P<units>\d{1,3}) units from(?: the)? completion of elective courses '
                         r'offered by ANU',
            'global_level': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,3}) '
                            r'units(?: (may|must|that) come)? from(?: the)?(?: completion of)?(?: '
                            r'further)? (?P<level>\d)0{3}-(?:level)?.*? courses(?P<subject_area> '
                            r'from the subject area [A-Z]{4}.*)?',
            'global_college': r'A minimum of (?P<units>\d{1,3}) units must come from completion '
                              r'of courses offered by the ANU College of (?P<college>[A-Z].*?)\.?$',
            'progression': r'(Students must achieve|Students who do not achieve)',
            'TEMPLATE': r'(?P<min>A minimum of )?(?P<max>A maximum of )?(?P<units>\d{1,3}) units '
                        r'from(?: the)? completion of courses from...',
            }
        requirement_text = node.flatten_requirement_text()

        # Single course taken multiple times. This has to be tested before a the single
        # compulsory course regex as they overlap enough that creating sufficiently discerning
        # regexes is fiddly.
        matches = re.match(principal_req_regex['single_course_multi'], requirement_text)
        if matches:
            operator = self._minmax_operator(matches)
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_ONE_TIMES_MANY'],
                    requirement_text,
                    int(matches.group('units')),
                    operator,
                    CourseListFilter([matches.group('code')])
                    )
            return new_order

        # Single compulsory course
        matches = re.match(principal_req_regex['single_course'], requirement_text)
        if matches:
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_SINGLE_COURSE'],
                    requirement_text,
                    int(matches.group('units')),
                    REQUIREMENT_OPERATORS['=='],
                    CourseListFilter([matches.group('code')])
                    )
            return new_order

        # Set of compulsory courses
        matches = re.match(principal_req_regex['compulsory_set'], requirement_text)
        if matches:
            child_text = self._extract_child_text(node)
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_COMPULSORY_COURSES'],
                    ' '.join([requirement_text, child_text]),
                    int(matches.group('units')),
                    REQUIREMENT_OPERATORS['=='],
                    CourseListFilter(re.findall(COURSE_REGEX, child_text))
                    )
            return new_order

        # Single study area
        matches = re.match(principal_req_regex['single_area'], requirement_text)
        if matches:
            operator = self._minmax_operator(matches)
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_MINMAX_SINGLE_AREA'],
                    requirement_text,
                    int(matches.group('units')),
                    operator,
                    CourseRegexFilter(build_course_regex(area_codes=[matches.group('area')]))
                    )
            return new_order

        # Multiple study areas
        matches = re.match(principal_req_regex['multiple_areas'], requirement_text)
        if matches:
            operator = self._minmax_operator(matches)
            child_text = self._extract_child_text(node)
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_MINMAX_MULTIPLE_AREAS'],
                    ' '.join([requirement_text, child_text]),
                    int(matches.group('units')),
                    operator,
                    CourseRegexFilter(
                            build_course_regex(area_codes=re.findall(AREA_REGEX, child_text))
                            )
                    )
            return new_order

        # Single set of courses. This must come after the "multiple_areas" regex due to
        # potential overlap.
        matches = re.match(principal_req_regex['single_set'], requirement_text)
        if not matches:
            matches = re.match(principal_req_regex['single_set2'], requirement_text)
        if matches:
            operator = self._minmax_operator(matches)
            child_text = self._extract_child_text(node)
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_SINGLE_SET'],
                    ' '.join([requirement_text, child_text]),
                    int(matches.group('units')),
                    operator,
                    CourseListFilter(re.findall(COURSE_REGEX, child_text))
                    )
            return new_order

        # Single subplan
        matches = re.match(principal_req_regex['single_subplan'], requirement_text)
        if matches:
            # Follow link to subplan and insert requirements for subplan...
            subplan_type = matches.group('subplan')
            subplan_title = matches.group('title')
            subplan_url = self.build_subplan_url(self.get_subplan_url(subplan_title, subplan_type))
            subplan_scraper = DegreeRuleScraper(subplan_url, header_id='requirements')
            return subplan_scraper.build_program_order_struct()

        # Subplan choice
        matches = re.match(principal_req_regex['subplan_choice'], requirement_text)
        if matches:
            # Create a container order, then get each subplan alternative and add its
            # ProgramOrder tree as a child of the container.
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PRINCIPAL_SUBPLAN_CHOICE'],
                    node.flatten_requirement_text(),
                    -1,
                    REQUIREMENT_OPERATORS['OR']
                    )
            subplan_type = matches.group('subplan')
            subplan_options = node.get_child_text_as_lines()
            for subplan_title in subplan_options:
                subplan_url = self.build_subplan_url(
                        self.get_subplan_url(subplan_title, subplan_type)
                        )
                subplan_scraper = DegreeRuleScraper(subplan_url, header_id='requirements')
                subplan_orders = subplan_scraper.build_program_order_struct()
                new_order.add_child(subplan_orders)
            return new_order

        # Specialisation co-requisite
        matches = re.match(principal_req_regex['specialisation_coreq'], requirement_text)
        if matches:
            # Follow link to subplan and insert requirements for subplan...
            subplan_type = 'major'
            subplan_title = matches.group('title')
            subplan_url = self.build_subplan_url(self.get_subplan_url(subplan_title, subplan_type))
            subplan_scraper = DegreeRuleScraper(subplan_url, header_id='requirements')
            return subplan_scraper.build_program_order_struct()

        # Specialisation co-requisite from alternatives
        matches = re.match(principal_req_regex['specialisation_coreq_choice'], requirement_text)
        if matches:
            # Create a container order, then get each subplan alternative and add its
            # ProgramOrder tree as a child of the container.
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['UGRAD_SPECIALISATION_COREQ_CHOICE'],
                    node.flatten_requirement_text(),
                    -1,
                    REQUIREMENT_OPERATORS['OR']
                    )
            subplan_type = 'major'
            subplan_options = node.get_child_text_as_lines()
            for subplan_title in subplan_options:
                subplan_url = self.build_subplan_url(
                        self.get_subplan_url(subplan_title, subplan_type)
                        )
                subplan_scraper = DegreeRuleScraper(subplan_url, header_id='requirements')
                subplan_orders = subplan_scraper.build_program_order_struct()
                new_order.add_child(subplan_orders)
            return new_order

        # Global requirement by course level (and maybe area as well).
        matches = re.match(principal_req_regex['global_level'], requirement_text)
        if matches:
            units = int(matches.group('units'))
            operator = self._minmax_operator(matches)
            # Extract course level and area details from the regex matches
            course_levels = re.findall(r'(\d)000', requirement_text)
            area_codes = None
            if matches.group('subject_area'):
                area_codes = re.findall(r'[A-Z]{4}', matches.group('subject_area'))
            course_filter = CourseRegexFilter(build_course_regex(area_codes, course_levels))
            return ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['GLOBAL_BY_LEVEL'],
                    requirement_text,
                    units,
                    operator,
                    course_filter
                    )

        # Global requirement by College.
        matches = re.match(principal_req_regex['global_college'], requirement_text)
        if matches:
            units = int(matches.group('units'))
            area_codes = COLLEGE_CODES[matches.group('college')]
            course_filter = CourseRegexFilter(build_course_regex(area_codes))
            return ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['GLOBAL_BY COLLEGE'],
                    requirement_text,
                    units,
                    REQUIREMENT_OPERATORS['MIN'],
                    course_filter
                    )

        # Electives
        matches = re.match(principal_req_regex['electives'], requirement_text)
        if matches:
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['ELECTIVES'],
                    requirement_text,
                    int(matches.group('units')),
                    REQUIREMENT_OPERATORS['=='],
                    CourseRegexFilter(r'.*')
                    )
            return new_order

        raise ValueError('Unknown principal requirement: {}'.format(requirement_text))

    def process_requirement_node(self, node: ReqNode) -> Iterable[ProgramOrder]:
        """
        Work out what to do with a given node in the requirements tree.

        Basically try a whole bunch regexs and branch depending on what matches.
        :param node: The ReqNode to process
        :return:
        """
        requirement_text = node.flatten_requirement_text()

        # Process globals
        if re.match(
                r'Th(e|is) (.*?) requires( the)? completion of (\d{1,3}) units.*?:?$',
                requirement_text
                ):
            return self.process_global_requirements(node)

        # Process principal requirements that follow additional global requirements
        elif re.match(r'The \d{1,3} units must consist of:$', requirement_text):
            return chain.from_iterable(
                    [self.process_requirement_node(child_node) for child_node in node.children]
                    )

        # Principal requirements, some double counting and split requirements will be caught by this
        elif re.match(
                r'(A maximum of |A minimum of )?\d{1,3} units( (may|must|that) come)? '
                r'from( the)?( completion of)?',
                requirement_text
                ):
            return [self.process_principal_requirement(node)]

        # Progression requirement
        elif re.match(
                r'(Students must achieve|Students who do not achieve)',
                requirement_text
                ):
            new_order = ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['PROGRESSION'],
                    requirement_text,
                    0,
                    REQUIREMENT_OPERATORS['==']
                    )
            return [new_order]

        # Specialisation corequisites
        elif re.match(r'This specialisation must be taken in conjunction with', requirement_text):
            return [self.process_principal_requirement(node)]

        elif re.match(r'Either:?$', requirement_text):
            return self.process_alternative_sets(node)

        # Or: statements were already processed by the Either: statement
        elif re.match(r'Or:?$', requirement_text):
            return []
        return [
            ProgramOrder(
                    self.plan_code,
                    ORDER_LABEL['UNKNOWN_REQUIREMENT'],
                    requirement_text,
                    0,
                    REQUIREMENT_OPERATORS['==']
                    )
            ]

    def build_program_order_struct(self) -> ProgramOrder:
        """
        Process the tree of ReqNodes to extract program orders and create a representation of linked
        ProgramOrders

        :return: Root of a tree of ProgramOrders representing the program/plan.
        """
        if not self.reqnode_tree.children:
            raise ValueError('Tried to build program orders from an empty requirements tree.')

        program_order_root = ProgramOrder(
                self.plan_code,
                '',
                '',
                -1,
                REQUIREMENT_OPERATORS['AND'],
                )
        for node in self.reqnode_tree.children:
            for new_order in self.process_requirement_node(node):
                program_order_root.add_child(new_order)

        return program_order_root
